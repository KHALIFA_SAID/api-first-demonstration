package com.binary.mindset.apiFirst.impl;


import com.binary.mindset.apiFirst.ProjectsApiDelegate;
import com.binary.mindset.apiFirst.model.Project;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectsApiImpl implements ProjectsApiDelegate {

    @Override
    public ResponseEntity<Project> getProject(Integer projectId) {

        Project project = new Project();
        project.setId(projectId);
        project.setFirstName("Example Said");
        project.setLastName("Example KHALIFA");

        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @Override
    public  ResponseEntity<List<Project>> getProjects(){
        List<Project> projects = new ArrayList<>();
        Project project1 = new Project();
        project1.setId(1);
        project1.setFirstName("Example meriem");
        project1.setLastName("Example slimeni");

        Project project2 = new Project();
        project2.setId(2);
        project2.setFirstName("Example Saido");
        project2.setLastName("Example khalifo");

        projects.add(project1);
        projects.add(project2);
        return new ResponseEntity<>(projects,HttpStatus.OK);
    }

}
