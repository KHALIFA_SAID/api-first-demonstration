package com.cloud.andro.souscription.api.servieImpl;

import com.cloud.andro.souscription.api.Entities.Address;
import com.cloud.andro.souscription.api.Repository.AddressRepository;

import com.cloud.andro.souscription.api.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;


import java.util.List;
@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressRepository addressRepository;



    @Override
    public List<Address> allAddresses() {
        return addressRepository.findAll();
    }

    @Override
    public Address getAddresse(Integer addressId) {
        return addressRepository.findById(addressId)
                .orElseThrow(() -> new ResourceNotFoundException("addressId " + addressId + " not found"));
    }

    @Override
    public Address create(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public Address update(Address addressRequest) {
        return addressRepository.save(addressRequest);
    }

    @Override
    public Boolean delete(Integer addressId) {
        return addressRepository.findById(addressId).map(paymentTerm -> {
            addressRepository.delete(paymentTerm);
            return true;
        }).orElseThrow(() -> new ResourceNotFoundException("paymentTermId " + addressId + " not found"));
    }
}
