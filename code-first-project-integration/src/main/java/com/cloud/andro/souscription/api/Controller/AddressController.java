package com.cloud.andro.souscription.api.Controller;

import com.cloud.andro.souscription.api.Entities.Address;
import com.cloud.andro.souscription.api.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = AddressController.ROOT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AddressController {

	public static final String ROOT_RESOURCE = "/api/addresses";

	public static final String ADDRESS_ID_PARAM = "addressId";

	public static final String CLIENT_ID_PARAM = "clientId";

	@Autowired
	private AddressService addressService;

	@GetMapping
	public ResponseEntity<?> allAddresses() {
		return ResponseEntity.ok(addressService.allAddresses());
	}

	@GetMapping(value = "/{" + AddressController.ADDRESS_ID_PARAM + "}")
	public Address getAddressById(@PathVariable(value = "addressId") Integer id) {
		return addressService.getAddresse(id);
	}

	@PostMapping("/{" + AddressController.CLIENT_ID_PARAM + "}")
	public ResponseEntity<Address> saveAddress(@PathVariable(value = "clientId") Integer clientId,
												   @RequestBody Address address) {
		return ResponseEntity.ok(addressService.create(address));
	}

	@PutMapping
	public ResponseEntity<Address> updateAddress(@RequestBody Address address) {
		return ResponseEntity.ok(addressService.update(address));
	}

	@DeleteMapping(value = "/{" + AddressController.ADDRESS_ID_PARAM + "}")
	public ResponseEntity<Boolean> deletePaymentTerm(@PathVariable(value = "addressId") Integer id) {
		return ResponseEntity.ok(addressService.delete(id));
	}

}
