package com.cloud.andro.souscription.api.Repository;


import com.cloud.andro.souscription.api.Entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AddressRepository extends JpaRepository<Address,Integer> {
}
