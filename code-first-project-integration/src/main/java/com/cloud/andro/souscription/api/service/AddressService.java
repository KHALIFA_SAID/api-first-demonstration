package com.cloud.andro.souscription.api.service;

import com.cloud.andro.souscription.api.Entities.Address;

import java.util.List;

public interface AddressService {
    public List<Address> allAddresses();

    public Address getAddresse(Integer addressId);

    public Address create(Address address);

    public Address update(Address address);

    public Boolean delete(Integer addressId);
}
