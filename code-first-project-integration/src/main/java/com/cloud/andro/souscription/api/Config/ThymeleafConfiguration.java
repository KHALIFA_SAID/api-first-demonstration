package com.cloud.andro.souscription.api.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
public class ThymeleafConfiguration {
		
		@Value("${source.base.folder}")
		private String sourceFolder;
		
	@Bean
	public ITemplateResolver templateResolver() {

		FileTemplateResolver templateResolver = new FileTemplateResolver();
		templateResolver.setPrefix(sourceFolder);
		templateResolver.setSuffix(".html");
		// HTML is the default value, added here for the sake of clarity.
		templateResolver.setTemplateMode("HTML");
		// Template cache is true by default. Set to false if you want
		// templates to be automatically updated when modified.
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine() {

		// SpringTemplateEngine automatically applies SpringStandardDialect and
		// enables Spring's own MessageSource message resolution mechanisms.
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		// templateEngine.setTemplateResolver(templateResolver());
		templateEngine.addTemplateResolver(templateResolver());
		return templateEngine;
	}

	
}
