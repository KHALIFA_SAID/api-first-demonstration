package com.cloud.andro.souscription.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CloudAndroSouscriptionApiApplication {


	public static void main(String[] args) {
        SpringApplication.run(CloudAndroSouscriptionApiApplication.class, args);
    }


}
