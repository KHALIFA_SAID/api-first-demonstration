package com.cloud.andro.souscription.api.Entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data @AllArgsConstructor @NoArgsConstructor 
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String addressType;
    private String streetNumber;
    private String streetName;
    private String streetType;
    private String addressComplement;
    private String bat_escalier;
    private String bp_lieudit;
    private String country;
    private String city;
    private String postalCode;

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", addressType=" + addressType +
                ", streetNumber='" + streetNumber + '\'' +
                ", streetName='" + streetName + '\'' +
                ", streetType='" + streetType + '\'' +
                ", addressComplement='" + addressComplement + '\'' +
                ", bat_escalier='" + bat_escalier + '\'' +
                ", bp_lieudit='" + bp_lieudit + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
