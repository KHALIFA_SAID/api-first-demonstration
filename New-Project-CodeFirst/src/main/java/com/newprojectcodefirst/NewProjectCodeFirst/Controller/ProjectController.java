package com.newprojectcodefirst.NewProjectCodeFirst.Controller;


import NewProjectCodeFirst.ProjectsApiDelegate;
import com.newprojectcodefirst.NewProjectCodeFirst.Entities.Project;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ProjectController implements ProjectsApiDelegate {
    @Override
    public ResponseEntity<Project> getProject(Integer projectId) {

        Project project = new Project();
        project.setId(projectId);
        project.setProjectName("Example nale of project");
        project.setSubject("Example Subject");

        return new ResponseEntity<>(project, HttpStatus.OK);
    }
}
