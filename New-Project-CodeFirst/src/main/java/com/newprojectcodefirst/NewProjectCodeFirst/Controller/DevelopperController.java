package com.newprojectcodefirst.NewProjectCodeFirst.Controller;

import com.newprojectcodefirst.NewProjectCodeFirst.Entities.Developper;
import com.newprojectcodefirst.NewProjectCodeFirst.Repositories.DevelopperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value =  "/api/developper")
public class DevelopperController {

    @Autowired
    private DevelopperRepository developperRepository;

    @GetMapping
    public ResponseEntity<?> allAddresses() {
        return ResponseEntity.ok(developperRepository.findAll());
    }

    @GetMapping(value = "/{id}")
    public Optional<Developper> getDevelopperById(@PathVariable(value = "id") Integer id) {
        return developperRepository.findById(id);
    }
}
