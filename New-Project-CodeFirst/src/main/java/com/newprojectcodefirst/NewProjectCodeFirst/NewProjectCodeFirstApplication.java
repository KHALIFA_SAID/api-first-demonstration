package com.newprojectcodefirst.NewProjectCodeFirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewProjectCodeFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewProjectCodeFirstApplication.class, args);
	}

}
