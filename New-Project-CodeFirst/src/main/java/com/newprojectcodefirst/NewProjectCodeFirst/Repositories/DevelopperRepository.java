package com.newprojectcodefirst.NewProjectCodeFirst.Repositories;

import com.newprojectcodefirst.NewProjectCodeFirst.Entities.Developper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface DevelopperRepository extends JpaRepository<Developper,Integer> {
}
